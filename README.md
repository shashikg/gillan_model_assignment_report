# gillan_model_assignment_report

Paper: Gillan CM, Kosinski M, Whelan R, Phelps EA & Daw ND (2016). Characterizing a psychiatric symptom dimension related to deficits in goal-directed control. eLife, 5:e11305

# Information about the files

- The directory: `individual_participant_data/` contains all the subjects experiment data.
- `MLE_Estimator.ipynb`: ipynb notebook file to estimate the parameters using MLE method.
- `param_estimate_mle.csv`: parameters estimated using MLE method.
- `MAP_Estimator.ipynb`: ipynb notebook file to estimate the parameters using MAP method.
- `param_estimate_map.csv`: parameters estimated using MAP method.
- `Posterior_Estimate.ipynb`: ipynb notebook file to find the posterior distribution using bayesian analysis and MCMC sampling. Notebook evaluated for one single subject. The result can be seen in the gitrepo by opening the ipynb file.
- 


# Brief Report
For any method, i.e., MLE, MAP, or Bayesian analysis (posterior estimation), we must need to formally write the likelihood of the two choices at both the stages of the experiment as a function of the six parameters, i.e., alpha, beta_stage2, beta_MB, beta_MF0, beta_MF1 as per the RL model described on P19-20 of the paper Gillan et al., (2016). Since the choice probabilities depend on the different Q-values. The first step was to compute the Q-tables as a function of the six parameters (well, only the alpha parameter out of the six parameters is involved in finding Q-values) using the actions, states, and rewards for each trial. After finding the corresponding Q-values at each time interval (Note here one time-interval is one trial), we can find the softmax probabilities of the two choices at both stages. Likelihood over all the trials will be multiplied to get the joint distribution (note that ideally, all trials will not be i.i.d. but it’s a decent assumption to ease the equation). After this, we can find the log-likelihood to carry out our further analysis. For MLE, I used scipy.optimise to minimize the negative likelihood function. For MAP, I used pymc3 to build the bayesian model.

I did the parameter estimation using all three methods. For MLE and MAP, the results are saved in `param_estimate_mle.csv` and `param_estimate_map.csv`. For full Bayesian analysis to estimate the posterior distribution, the running time was high because of MCMC sampling. Therefore I have reported the population parameter only for one subject. The reason I tried out all the estimation methods because that’s gives a check on whether the modeling part is correct or not. MLE could give the issue of overfitting while the MAP estimate can get poor because of a bad prior. But if the estimate from both MLE and MAP are similar then it gives us more confidence in the estimates. For the given data, most of the cases yielded close enough values for both MLE and MAP except for some. The posterior estimation using bayesian analysis gives the options to look at predicted posterior distribution. If the predicted posterior from multiple chains turn out to be close to each other that means the model is a good approximation of the given data, for example, all the 4 chains in the shown example for the bayesian estimate are close to each other.

**For MLE, I applied a bound of** `0 to 1` **on** `alpha` **and** `0 to inf` **on all the** `beta’s`. 

**The prior assumption for MAP and posterior estimation**

- I took Beta(1, 1) for the `alpha` parameter, which gives equiprobable values for alpha between 0 to 1. Since alpha ranges between 0 to 1, this is a good assumption on prior.
- For all the `beta’s`, I took half normal of a normal distribution whose mean is at 0 and std is 20. I assumed beta should be positive.

The result for the Posterior prediction is shown inside the .ipynb notebook.


